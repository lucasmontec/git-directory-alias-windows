@ECHO OFF
color A

echo Installing...
echo -

if exist "%userprofile%"/.aliases_loader (
    echo Already installed!
	pause
) else (
    copy .aliases_loader "%userprofile%"
	type append >> "%userprofile%"/.bashrc
	echo installed!
	pause
)

