@ECHO OFF
color A

echo Installing...
echo -

if exist "%userprofile%/.bashrc_backup" (
	del "%userprofile%"/.bashrc
	move "%userprofile%"/.bashrc_backup "%userprofile%/.bashrc"
    echo Uninstalled!
	pause
) else (
	echo Already removed!
	pause
)

