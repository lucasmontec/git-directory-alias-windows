# Git Aliases Per-Directory

This is a simple script that instructs your git bash to load an aliases file
(actually you can put any commands in your file) when your git bash is loaded.

The script looks for an .aliases file in your current directory (where the bash
is loaded) and runs that file to load any aliases, execute commands or load
git commands.

There is no security, your security should be working with a good team.

## How to use it

1. Download the entire repository in a directory (click [here](https://gitlab.com/lucasmontec/git-directory-alias-windows/-/archive/master/git-directory-alias-windows-master.zip)).
2. To install it, just run install.bat.
3. Create a .alias file in your local git repo with the aliases you want.
4. Open git bash (git for windows) on that directory.
5. Enjoy.

To remove the script just run uninstall.bat.