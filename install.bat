@ECHO OFF
color A

echo Installing...
echo -

if exist "%USERPROFILE%/.bashrc_backup" (
    echo Already installed!
	pause
) else (
	copy /Y "%USERPROFILE%\.bashrc" "%USERPROFILE%\.bashrc_backup"
	type append >> "%USERPROFILE%"/.bashrc
	echo installed!
	pause
)

